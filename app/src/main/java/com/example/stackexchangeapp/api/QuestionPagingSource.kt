package com.example.stackexchangeapp.api

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.stackexchangeapp.model.Question
import javax.inject.Inject

class QuestionPagingSource @Inject constructor(private val stackAPI: StackAPI) :
    PagingSource<Int, Question>() {
    private val TAG = "QuestionPagingSource"
    override fun getRefreshKey(state: PagingState<Int, Question>): Int {
        return 1
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Question> {
        val page = params.key ?: 1
        val response = try {
            stackAPI.getRecentQuestions(page = page).items
        } catch (e: Exception) {
            Log.d(TAG, "load: error = ${e.message}")
            emptyList()
        }
        return try {
            LoadResult.Page(data = response, prevKey = if(page==1) 1 else page - 1, nextKey = page + 1)
        } catch (e: Exception) {
            Log.d(TAG, "load: failed, " + e.message)
            LoadResult.Error(e)
        }
    }
}