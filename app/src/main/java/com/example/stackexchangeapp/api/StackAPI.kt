package com.example.stackexchangeapp.api

import com.example.stackexchangeapp.model.Question
import com.example.stackexchangeapp.model.ResponseWrapper
import retrofit2.http.GET
import retrofit2.http.Query

interface StackAPI {
    @GET("/2.2/questions")
    suspend fun getRecentQuestions(
        @Query("page") page: Int = 1,
        @Query("order") order: String = "desc",
        @Query("sort") sort: String = "activity",
        @Query("site") site: String = "stackoverflow",
        @Query("filter") filter: String = "!nMp.SvMbcA"
    ): ResponseWrapper<Question>
}