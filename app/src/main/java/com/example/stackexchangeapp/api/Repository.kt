package com.example.stackexchangeapp.api

import androidx.lifecycle.LiveData
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.liveData
import com.example.stackexchangeapp.Constants
import com.example.stackexchangeapp.model.Question
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Repository @Inject constructor(var stackAPI: StackAPI) {
    fun getQuestions(): LiveData<PagingData<Question>> =
        Pager(config = PagingConfig(
            pageSize = Constants.PAGE_SIZE,
            enablePlaceholders = false
        ),
            pagingSourceFactory = { QuestionPagingSource(stackAPI) }
        ).liveData
}