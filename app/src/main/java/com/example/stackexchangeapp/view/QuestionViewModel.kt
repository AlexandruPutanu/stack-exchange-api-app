package com.example.stackexchangeapp.view

import androidx.lifecycle.ViewModel
import com.example.stackexchangeapp.api.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class QuestionViewModel @Inject constructor(var repository: Repository) : ViewModel() {
    fun getQuestions() = repository.getQuestions()
}