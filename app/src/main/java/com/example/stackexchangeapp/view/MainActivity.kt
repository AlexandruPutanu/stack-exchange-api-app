package com.example.stackexchangeapp.view

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.stackexchangeapp.R
import com.example.stackexchangeapp.model.Question
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import java.lang.Exception

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private val TAG = "MainActivity"
    private lateinit var mRecyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mRecyclerView = findViewById(R.id.recyclerView)
        mRecyclerView.layoutManager = LinearLayoutManager(this)
        mRecyclerView.adapter = QuestionAdapter()
        val viewmodel by viewModels<QuestionViewModel>()
        val pagingAdapter = QuestionAdapter()
        mRecyclerView.adapter = pagingAdapter
            viewmodel.getQuestions()
                .observe(this@MainActivity
                ) { t ->
                    if (t != null) {
                        Log.d(TAG, "onCreate: viewmodel getQuestion = $t")
                        (mRecyclerView.adapter as QuestionAdapter).submitData(lifecycle, t)
                    }
                }
    }
}