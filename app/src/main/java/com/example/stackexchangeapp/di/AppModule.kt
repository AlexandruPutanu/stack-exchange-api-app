package com.example.stackexchangeapp.di

import com.example.stackexchangeapp.Constants
import com.example.stackexchangeapp.api.StackAPI
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit {
        val gson: Gson = GsonBuilder().create()
        return Retrofit.Builder()
            .baseUrl(Constants.API_SITE)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @Singleton
    @Provides
    fun provideStackAPI(retrofit: Retrofit): StackAPI {
        return retrofit.create(StackAPI::class.java)
    }
}