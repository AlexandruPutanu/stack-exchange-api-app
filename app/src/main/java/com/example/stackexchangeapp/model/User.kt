package com.example.stackexchangeapp.model

data class User(
    val display_name: String?,
    val profile_image: String?,
    val link: String?
)
