package com.example.stackexchangeapp.model

import android.os.Build
import android.text.Html
import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter

data class Question(
    val body: String,
    val link: String,
    val view_count: Int,
    val owner: User?,
    val title: String,
    val creation_date: Long,
    val score: Int,
    val is_answered: Boolean,
    val tags: List<String>
)

@SuppressWarnings
@BindingAdapter("htmlBody")
fun parseHtmlBody(view: View, body: String) {
    if (view is TextView) {
        view.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            Html.fromHtml(body, Html.FROM_HTML_MODE_COMPACT).toString()
        else
            Html.fromHtml(body).toString()
    }
}

@BindingAdapter("tagList")
fun parseTagList(view: View, list: List<String>) {
    if (view is TextView) {
        val builder: StringBuilder = java.lang.StringBuilder()
        builder.append("Tags: ")
        for (tag in list) {
            builder.append(tag)
            builder.append(", ")
        }
        builder.delete(builder.length - 2, builder.length)
        view.text = builder.trim()
    }
}