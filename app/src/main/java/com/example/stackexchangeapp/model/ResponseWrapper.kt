package com.example.stackexchangeapp.model

data class ResponseWrapper<T>(val items: List<T>)