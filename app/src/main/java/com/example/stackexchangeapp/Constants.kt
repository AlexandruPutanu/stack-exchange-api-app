package com.example.stackexchangeapp

object Constants {
    const val API_SITE = "https://api.stackexchange.com"
    const val PAGE_SIZE = 30
}